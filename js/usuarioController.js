function login(){
    let usuario = {
        usuario: $('#usuario').val(),
        password: $('#password').val(),
        metodo: "login"
    };
    $.ajax({
        url: "../php/usuario.php",
        data: usuario,
        method: 'POST',
        error: function(xhr){
            alert("An error ocurred: " + xhr.status + " " + xhr.statusText);
        },
        beforeSend: function(){
            $('.btn-primary').val('Ingresando...');
        },
        success: function (usuario_response) {
            if (usuario_response == "Error") {
                $('#mensaje').text("El usuario digitado no existe");
            } else {
                let usuarioGuardado = JSON.parse(usuario_response);
                if (usuarioGuardado.contrasena === usuario.password) {
                    const loginBitacora = (async () => {
                        const res = await fetch('../php/usuario.php?metodo=agregarBitacora&idusuario='+usuarioGuardado.idusuarios);
                        const respuesta = await res.json();
                    })();
                    sessionStorage.setItem("usuarioLogueado",usuario_response);
                    usuarioGuardado.rol == '0' ? window.location.href = "admin.html" : window.location.href = "index.html";
                } else {
                    $('#mensaje').fadeIn('slow').text("El password es incorrecto").css('color','red');
                    setTimeout(function(){
                        $('#mensaje').fadeOut('slow').text("El password es incorrecto").css('color','red');
                    },3000);
                    $('.btn-primary').val('Ingresar');
                }
            }
        }
    });
    return false;
}

function logout() {
    sessionStorage.removeItem('usuarioLogueado');
    window.location.href = 'index.html';
}

function contacto (){
    let usuario = {
        nombre: $('#nombre').val(),
        correo: $('#correo').val(),
        telefonoCas: $('#telefonoCas').val(),
        telefonoCel: $('#telefonoCel').val(),
        mensaje: $('#mensaje2').val(),
        metodo: "registro"
    };

    let nombre,correo,telefonoCas,telefonoCel,mensaje,expresion;
    nombre = $('#nombre').val();
    correo = $('#correo').val();
    telefonoCas = $('#telefonoCas').val();
    telefonoCel = $('#telefonoCel').val();
    mensaje = $('#mensaje2').val();
    expresion = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    
    if(nombre === "" || correo === "" || telefonoCas === "" || telefonoCel === "" || mensaje === "" ){
        $('#mensaje').text('*Todos los campos son obligatorios').css({
            'color': 'red',
            'text-align':'center'
        });
        return false;
    }else if(nombre.length > 45){
        $('#validarNombre').fadeIn('slow').text('*El campo nombre no debe ser mayor que 45 caracteres').css('color','red');
        setTimeout(function () {
            $('#validarNombre').fadeOut('slow').text('*El campo nombre no debe ser mayor que 45 caracteres').css('color', 'red');
        }, 3000);
        return false;
    }else if(!expresion.test(correo)){
        $('#validarCorreo').fadeIn('slow').text('*El correo que ingresastes no es válido').css('color', 'red');
        setTimeout(function () {
            $('#validarCorreo').fadeOut('slow').text("*El correo que ingresastes no es válido").css('color', 'red');
        }, 3000);
        return false;
    }else if (telefonoCas.length > 45) {
        $('#validarTel').fadeIn('slow').text('*El campo télefono casa no debe ser mayor que 45 caracteres').css('color', 'red');
        setTimeout(function () {
            $('#validarTel').fadeOut('slow').text("*El campo télefono casa no debe ser mayor que 45 caracteres").css('color', 'red');
        }, 3000);
        return false;
    }else if(telefonoCel.length > 45){
        $('#validarCel').fadeIn('slow').text('*El campo Celular no debe ser mayor que 45 caracteres').css('color', 'red');
        setTimeout(function () {
            $('#validarCel').fadeOut('slow').text("*El campo Celular no debe ser mayor que 45 caracteres").css('color', 'red');
        }, 3000);
        return false;
    }else if(isNaN(telefonoCas)){
        $('#validarTel').fadeIn('slow').text('*El campo télefono casa no es número').css('color', 'red');
        setTimeout(function () {
            $('#validarTel').fadeOut('slow').text("*El campo télefono casa no es número").css('color', 'red');
        }, 3000);
        return false;
    }else if(isNaN(telefonoCel)){
        $('#validarCel').fadeIn('slow').text('*Los datos que ingresastes no es un número').css('color', 'red');
        setTimeout(function () {
            $('#validarCel').fadeOut('slow').text("*Los datos que ingresastes no es un número").css('color', 'red');
        }, 3000);
        return false;
    }else if(mensaje === ""){
        $('#validarMsj').fadeIn('slow').text('*No debes llenar el campo mensaje').css('color','red');
        setTimeout(function () {
            $('#validarMsj').fadeOut('slow').text("*Los datos que ingresastes no es un número").css('color', 'red');
        }, 3000);
        return false;
    }
    $.ajax({
        url: "../php/usuario.php",
        data: usuario,
        method: 'POST',
        error: function(xhr){
            alert("An error ocurred: " + xhr.status + " " + xhr.statusText);
        },
        beforeSend: function(){
            $('.btn-primary').val('Registrando...');
        },
        success: function (usuario_response) {
            if (usuario_response == 'Exito') {
                $('#mensaje').text('Se ha registrado con éxito').css({
                    'color':'green',
                    'text-align': 'center'
                });
                $('.btn-primary').val('Registrar');
            }
        }
    });
    return false;
}

function listarUsuarios(){
    let listar = {
        metodo: "listar"
    };
    $.ajax({
        url: "../php/usuario.php",
        data: listar,
        method: 'POST',
        error: function(xhr){
            console.log("An error ocurred: " + xhr.status + " " + xhr.statusText);
        },
        success: function (usuario_response) {
            let usuarios = JSON.parse(usuario_response);
            usuarios.map(function (e) {
                let tr = document.createElement('tr');
                let nombre = document.createElement('td');
                $(nombre).text(e.nombre);
                $(nombre).attr('class','table__users');
                $(tr).append(nombre);
                let correo = document.createElement('td');
                $(correo).text(e.correo);
                $(correo).attr('class','table__users');
                $(tr).append(correo);
                let telefonoCas = document.createElement('td');
                $(telefonoCas).text(e.telefonoCas);
                $(telefonoCas).attr('class','table__users');
                $(tr).append(telefonoCas);
                let telefonoCel = document.createElement('td');
                $(telefonoCel).text(e.telefonoCel);
                $(telefonoCel).attr('class','table__users');
                $(tr).append(telefonoCel);
                let mensaje = document.createElement('td');
                $(mensaje).text(e.mensaje);
                $(mensaje).attr('class','table__users');
                $(tr).append(mensaje);
                $('#tabla').append(tr);
            })
        }
    });
    return false;
}