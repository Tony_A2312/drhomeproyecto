var usuarioGuardado = sessionStorage.getItem('usuarioLogueado');
if (usuarioGuardado) {
    usuarioGuardado = JSON.parse(usuarioGuardado);
    $('#login').text(usuarioGuardado.usuario);
    $('#botonLogin').addClass('dropdown');
    $('#login').addClass('dropdown-toggle');
    $('#login').attr('href','#');
    $('#login').attr('data-toggle','dropdown');
    $('#login').attr('aria-haspopup','true');
    $('#login').attr('aria-expanded','false');
    $('<div class="dropdown-menu dropdown-menu-right" aria-labelledby="login"><a class="dropdown-item" href="admin.html">Admin</a><a class="dropdown-item" href="#" onclick="logout()">Logout</a></div>').appendTo('#botonLogin');
}