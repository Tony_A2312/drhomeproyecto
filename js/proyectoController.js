function catalogo() {
    let proyecto = {
        metodo: "listar"
    };
    $.ajax({
        url: "../php/proyectos.php",
        method: "POST",
        data: proyecto,
        error:  function (xhr) {
            console.log("An error ocurred: " + xhr.status + " " + xhr.statusText);
        },
        success: function (proyecto_response) {
            let proyectos = JSON.parse(proyecto_response);
            $('#catalogoContainer').hide();
            let contador = 0;
            let numPagina = 1;
            proyectos.map(function (proyecto) {
                $('<div id="' + proyecto.proyectos + '" class="col-lg-4 col-sm-6 portfolio-item pagina' + numPagina + '"><div class="card h-100"><a href="#' + proyecto.proyectos + '"><img class="card-img-top" src="../img/' + proyecto.imagen + '" alt=""></a><div class="card-body"><h4 class="card-title"><a href="#" class="nombre">' + proyecto.titulo + '</a></h4><p class="card-text" class="descripcion">' + proyecto.descripcion + '</p></div></div></div>').appendTo('#catalogoContainer')

                contador++
                if (contador == 6) {
                    contador = 0;
                    numPagina++
                }
            })
            $('.portfolio-item').hide();
            $('.pagina1').show();
            $('#catalogoContainer').show();
            let totalPaginas = Math.ceil(proyectos.length / 6);
            for (let i = 1; i <= totalPaginas; i++) {
                $(' <li class="page-item"><a class="page-link" onclick="paginacion(' + i + ')">' + i + '</a></li>').appendTo('.pagination')
            }
        }
    })
}
function logout() {
    sessionStorage.removeItem('usuarioLogueado');
    window.location.href = 'proyectosCatalogo.html';
}
function paginacion (numero) {
    $('.portfolio-item').hide();
    $('.pagina' + numero).show();
}
$('#proyecto').on('submit', (e) => {
    e.preventDefault();
    let datos = new FormData($("#proyecto")[0]);
    $.ajax({
        url: "../php/proyectos.php",
        type: 'POST',
        data: datos,
        contentType: false,
        processData: false,
        success: function (datos) {
            $('#respuesta').html(datos).css('color', 'green');
        }
    })
});
