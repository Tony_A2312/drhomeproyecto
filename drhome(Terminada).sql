CREATE DATABASE  IF NOT EXISTS `drhome` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `drhome`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: drhome
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.31-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bitacora`
--

DROP TABLE IF EXISTS `bitacora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bitacora` (
  `idbitacora` int(11) NOT NULL AUTO_INCREMENT,
  `usuarios_idusuarios` varchar(45) DEFAULT NULL,
  `accion` varchar(45) DEFAULT NULL,
  `fecha` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idbitacora`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bitacora`
--

LOCK TABLES `bitacora` WRITE;
/*!40000 ALTER TABLE `bitacora` DISABLE KEYS */;
INSERT INTO `bitacora` VALUES (1,'1','el usuario se logueo','23/04/2018'),(2,'1','el usuario se logueo','23/04/2018'),(3,'1','el administrador subio un proyecto','23/04/2018'),(4,'1','el administrador subio un proyecto','23/04/2018'),(5,'1','el administrador subio un proyecto','23/04/2018'),(6,'1','el administrador subio un proyecto','23/04/2018'),(7,'1','el administrador subio un proyecto','23/04/2018'),(8,'1','el administrador subio un proyecto','23/04/2018'),(9,'1','el administrador subio un proyecto','23/04/2018'),(10,'1','el administrador subio un proyecto','23/04/2018'),(11,'1','el administrador subio un proyecto','23/04/2018'),(12,'1','el administrador subio un proyecto','23/04/2018'),(13,'1','el administrador subio un proyecto','23/04/2018'),(14,'1','el administrador subio un proyecto','23/04/2018'),(15,'1','el administrador subio un proyecto','23/04/2018'),(16,'1','el administrador subio un proyecto','23/04/2018'),(17,'1','el administrador subio un proyecto','23/04/2018'),(18,'1','el administrador subio un proyecto','23/04/2018'),(19,'1','el administrador subio un proyecto','23/04/2018'),(20,'1','el administrador subio un proyecto','23/04/2018'),(21,'1','el administrador subio un proyecto','23/04/2018'),(22,'1','el administrador subio un proyecto','23/04/2018'),(23,'1','el administrador subio un proyecto','23/04/2018'),(24,'1','el administrador subio un proyecto','23/04/2018'),(25,'1','el administrador subio un proyecto','23/04/2018'),(26,'1','el administrador subio un proyecto','23/04/2018'),(27,'1','el administrador subio un proyecto','23/04/2018'),(28,'1','el usuario se logueo','23/04/2018'),(29,'1','el usuario se logueo','23/04/2018'),(30,'1','el usuario se logueo','29/04/2018'),(31,'1','el usuario se logueo','29/04/2018');
/*!40000 ALTER TABLE `bitacora` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mensajes`
--

DROP TABLE IF EXISTS `mensajes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mensajes` (
  `idmensajes` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` varchar(45) DEFAULT NULL,
  `mensaje` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idmensajes`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mensajes`
--

LOCK TABLES `mensajes` WRITE;
/*!40000 ALTER TABLE `mensajes` DISABLE KEYS */;
INSERT INTO `mensajes` VALUES (1,'2018-04-22 20:58:13','El mensaje se recibio'),(2,'2018-04-22 20:59:11','El mensaje se recibio'),(3,'2018-04-22 21:05:35','El mensaje se recibio');
/*!40000 ALTER TABLE `mensajes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyectos`
--

DROP TABLE IF EXISTS `proyectos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proyectos` (
  `proyectos` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) DEFAULT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `imagen` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`proyectos`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyectos`
--

LOCK TABLES `proyectos` WRITE;
/*!40000 ALTER TABLE `proyectos` DISABLE KEYS */;
INSERT INTO `proyectos` VALUES (1,'Proyecto #1','Proyecto terminado en Santa Ana','../img/0a6d8913118999f4f4deef6ad85290bc'),(2,'Proyecto #2','Proyecto terminado en Santa Ana','../img/dcb3706f296ad6cc376dcd61226f9211'),(3,'Proyecto #3','Proyecto terminado en Santa Ana','../img/ee69290882be946ef3663ec485d9c0db'),(4,'Proyecto #4','Proyecto terminado en Santa Ana','../img/4723b2d2e2b48436a002350dd5910d5b'),(5,'Proyecto #5','Proyecto terminado en Santa Ana','../img/9b87c4f25ae0ecb653418f35c4daa94c'),(6,'Proyecto #6','Proyecto terminado en Dos Cercas','../img/54e2c8d45645bef956d693893050bed6'),(7,'Proyecto #7','Proyecto terminado en Dos Cercas','../img/69f1ab47b6ba272bd6810325d9a7e488'),(8,'Proyecto #8','Proyecto terminado en Dos Cercas','../img/b9f699b2e7651380c719659d55ce82d1'),(9,'Proyecto #9','Proyecto terminado en Dos Cercas','../img/36750190df8e0691b46649abea31e6be'),(10,'Proyecto #10','Proyecto terminado en Dos Cercas','../img/10ff64fad0702fdfdb7e50c76157103c'),(11,'Proyecto #11','Proyecto terminado en San Sebastian','../img/f76f2d6ef5edac1110d3f8b60c7f85b9'),(12,'Proyecto #12','Proyecto terminado en San Sebastian','../img/42b4d8c01a9dd032b93e8984c4aa6dff'),(13,'Proyecto #13','Proyecto terminado en San Sebastian','../img/00954a78ea26b156540eac86fdca053b'),(14,'Proyecto #14','Proyecto terminado en San Sebastian','../img/befb6412c26af2bda1c4d406392f8ff2'),(15,'Proyecto #15','Proyecto terminado en San Sebastian','../img/0a3471b8f6b8ae41c1d3413e0e7c9952'),(16,'Proyecto #16','Proyecto terminado en San Sebastian','../img/ad6575f852e7c3ddc34e7d2ca38366db'),(17,'Proyecto #17','Proyecto terminado en San Antonio de Belen','../img/042dfd7d66757c2433e882eeafe74a1b'),(18,'Proyecto #18','Proyecto terminado en San Antonio de Belen','../img/9aacab4a74dcf231dffeafcf863735b3'),(19,'Proyecto #19','Proyecto terminado en San Antonio de Belen','../img/513432068563e8ca1affb6155a99ff9c'),(20,'Proyecto #20','Proyecto terminado en San Antonio de Belen','../img/50ab85e0eaaa86b2a1416e9cc811b1b2'),(21,'Proyecto #21','Proyecto terminado en San Antonio de Belen','../img/e6dac9c8f19f49b64dcfc6c1dc82394c'),(22,'Proyecto #22','Proyecto terminado en Alajuelita,centro','../img/f085666f213d1a8cc3fc5c703fffb056'),(23,'Proyecto #23','Proyecto terminado en Alajuelita,centro','../img/f0e61732399e0e114b663a5c4082ff7a'),(24,'Proyecto #24','Proyecto terminado en Alajuelita,centro','../img/80f47405e5d8046036938a7780e658b4'),(25,'Proyecto #25','Proyecto terminado en Alajuelita,centro','../img/e7034d43646368f311b8e64b7fac50ff');
/*!40000 ALTER TABLE `proyectos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `telefonoCas` varchar(45) DEFAULT NULL,
  `telefonoCel` varchar(45) DEFAULT NULL,
  `mensaje` varchar(500) DEFAULT NULL,
  `usuario` varchar(45) DEFAULT NULL,
  `contrasena` varchar(45) DEFAULT NULL,
  `rol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idusuarios`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'Bryan ','bcloaiza@gmail.com','22703483','63837387',NULL,'DrHome','drhomelimon1994','0');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER InsertarMensajes2 AFTER INSERT ON usuarios
FOR EACH ROW 
INSERT INTO mensajes(idmensajes,fecha,mensaje)
VALUES(null,NOW(),'El mensaje se recibio') */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping events for database 'drhome'
--

--
-- Dumping routines for database 'drhome'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-28 17:55:10
